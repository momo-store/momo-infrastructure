#--------------------------------------------------------------------------------
#Variable section
#--------------------------------------------------------------------------------
variable "YC_CLOUD_ID" {
    type=string
}
variable "YC_FOLDER_ID" {
    type=string
}
variable "YC_TOKEN" {
    type=string
}

variable "SSH_PUBLIC_KEY" {
    type=string
}

variable "ACCESS_KEY_ID" {
    type=string
}
variable "SECRET_ACCESS_KEY" {
    type=string
}

