terraform {
   backend "s3" {
      endpoint   = "storage.yandexcloud.net"
      bucket = "momo-terraform"
      region     = "us-east-1"
      key        = "k8s.tfstate"

      skip_region_validation      = true
      skip_credentials_validation = true
   }
}

