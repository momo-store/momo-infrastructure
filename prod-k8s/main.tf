#--------------------------------------------------------------------------------
#Main section
#--------------------------------------------------------------------------------
resource "yandex_kubernetes_cluster" "zonal_cluster_resource_name" {
  name        = "prod-cluster"
  description = "cluster 1x1"

  network_id = "enpufjpanfsd3d2isb1e"

  master {
    version = "1.22"
    zonal {
      zone      = "ru-central1-a"
      subnet_id = "e9b6ram6rhouugts3i6v"
    }

    public_ip = true

    maintenance_policy {
      auto_upgrade = true

      maintenance_window {
        start_time = "15:00"
        duration   = "3h"
      }
    }
  }

  service_account_id      = "ajeod4r55skho7l9ao4t"
  node_service_account_id = "ajeod4r55skho7l9ao4t"

  release_channel = "REGULAR"

  kms_provider {
    key_id = "abj1du58i2u75lv19eak"
  }
}

resource "yandex_kubernetes_node_group" "my_node_group" {
  cluster_id  = "${yandex_kubernetes_cluster.zonal_cluster_resource_name.id}"
  name        = "prod-node-group"
  description = "cluster 1x1"
  version     = "1.22"

  labels = {
    "app" = "momo-store"
  }

  instance_template {
    platform_id = "standard-v2"

    network_interface {
      nat                = true
      subnet_ids         = ["e9b6ram6rhouugts3i6v"]
    }

    resources {
      memory = 4
      cores  = 4
    }

    boot_disk {
      type = "network-hdd"
      size = 64
    }

    scheduling_policy {
      preemptible = false
    }

    container_runtime {
      type = "containerd"
    }
  }

  scale_policy {
    fixed_scale {
      size = 2
    }
  }

  allocation_policy {
    location {
      zone = "ru-central1-a"
    }
  }

  maintenance_policy {
    auto_upgrade = true
    auto_repair  = true

    maintenance_window {
      day        = "monday"
      start_time = "15:00"
      duration   = "3h"
    }

    maintenance_window {
      day        = "friday"
      start_time = "10:00"
      duration   = "4h30m"
    }
  }
}

#--------------------------------------------------------------------------------
#Output section
#--------------------------------------------------------------------------------
output "cluster_external_v4_endpoint" {
  value = yandex_kubernetes_cluster.zonal_cluster_resource_name.master.0.external_v4_endpoint
}

output "my_node_group_status" {
  value = yandex_kubernetes_node_group.my_node_group.status
}

#--------------------------------------------------------------------------------
#Terraform section
#--------------------------------------------------------------------------------
terraform {
 required_providers {
  yandex = {
  source  = "terraform-registry.storage.yandexcloud.net/yandex-cloud/yandex"
  version = ">=0.13"
  }
 }
}

