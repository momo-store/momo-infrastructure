terraform {
   backend "s3" {
      endpoint   = "storage.yandexcloud.net"
      bucket = "momo-terraform"
      region     = "us-east-1"
      key        = "infra.tfstate"

      skip_region_validation      = true
      skip_credentials_validation = true
   }
}
