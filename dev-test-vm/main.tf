#--------------------------------------------------------------------------------
#Main section
#--------------------------------------------------------------------------------
resource "yandex_compute_instance" "vm-1" {
    name = "dev-test-vm"

    resources {
        cores  = 4
        memory = 4
    }

    boot_disk {
        initialize_params {
            image_id = "fd8fte6bebi857ortlja"
            size        = 50
        }
    }

    network_interface {
        subnet_id = "e9b6ram6rhouugts3i6v"
        nat       = true
    }

    metadata = {
	user-data = "#cloud-config\nusers:\n  - name: ansible\n    groups: sudo\n    shell: /bin/bash\n    sudo: ['ALL=(ALL) NOPASSWD:ALL']\n    ssh-authorized-keys:\n      - ${var.SSH_PUBLIC_KEY}\n"
        ssh-keys = "ubuntu:${var.SSH_PUBLIC_KEY}"
    }
}

#--------------------------------------------------------------------------------
#Output section
#--------------------------------------------------------------------------------
output "ip_address" {
    value = yandex_compute_instance.vm-1.network_interface.0.ip_address
}

output "ext_ip_address" {
    value = yandex_compute_instance.vm-1.network_interface.0.nat_ip_address
}

#--------------------------------------------------------------------------------
#Terraform section
#--------------------------------------------------------------------------------
terraform {
 required_providers {
  yandex = {
  source  = "terraform-registry.storage.yandexcloud.net/yandex-cloud/yandex"
  version = ">=0.13"
  }
 }
}
